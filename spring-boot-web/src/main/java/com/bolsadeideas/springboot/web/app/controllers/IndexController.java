package com.bolsadeideas.springboot.web.app.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bolsadeideas.springboot.web.app.models.Usuario;

@Controller
@RequestMapping("/app")
public class IndexController {
	
	/**
	@RequestMapping(value="/index", method=RequestMethod.GET)
	public String index() {
		return "index";
	}
	**/
	
	@Value("${texto.indexcontroller.index.titulo}")
	private String textoIndex;
	@Value("${texto.indexcontroller.perfil.titulo}")
	private String textoPerfil;
	@Value("${texto.indexcontroller.listar.titulo}")
	private String textoListar;
	
	@GetMapping({"/index", "/", "", "/home"})
	public String index(Model model) {
		model.addAttribute("titulo", textoIndex);
		return "index";
	}
	
	@RequestMapping("/perfil")
	public String perfil(Model model) {
		
		Usuario usuario = new Usuario();
		usuario.setNombre("Esteban");
		usuario.setApellido("Morales");
		usuario.setEmail("correo@correo.com");
	
		model.addAttribute("usuario", usuario);
		model.addAttribute("titulo", textoPerfil.concat(usuario.getNombre()));
		
		return "perfil";
	}
	
	@RequestMapping("/listar")
	public String listar(Model model) {
		

		/**
		List<Usuario> usuarios = new ArrayList<>();
		usuarios.add(new Usuario("Andrés","Guzman","Andres@correo.com"));
		usuarios.add(new Usuario("John","Doe","Jhon@correo.com"));		
		usuarios.add(new Usuario("jane","Doe","Jane@correo.com"));
		**/

		model.addAttribute("titulo", textoListar);

		return "listar";
		
	}
	
	@ModelAttribute("usuarios")
	public List<Usuario> poblarUsuario() {
		List<Usuario> usuarios = Arrays.asList(new Usuario("Andrés","Guzman","Andres@correo.com"),
				new Usuario("John","Doe","Jhon@correo.com"),
				new Usuario("jane","Doe","Jane@correo.com"),
				new Usuario("Tornado","Roe","Tornado@correo.com"));
		return usuarios;
	}
	

}
 